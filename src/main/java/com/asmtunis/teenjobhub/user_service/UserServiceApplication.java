package com.asmtunis.teenjobhub.user_service;

import com.asmtunis.teenjobhub.user_service.services.elastic.ElasticUserProfileRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableElasticsearchRepositories(basePackages = "com.asmtunis.teenjobhub.user_service.services.elastic")
@EnableMongoRepositories(excludeFilters = @ComponentScan.Filter(value = ElasticUserProfileRepository.class, type = FilterType.ASSIGNABLE_TYPE))
@EnableDiscoveryClient
public class UserServiceApplication {


    public static void main(String[] args) {
        SpringApplication.run(UserServiceApplication.class, args);
    }

}

