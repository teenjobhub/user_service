package com.asmtunis.teenjobhub.user_service.events.chat;

import com.asmtunis.teenjobhub.user_service.services.notification.NotificationPublisher;
import com.asmtunis.teenjobhub.user_service.services.profile.ProfileService;
import org.springframework.context.event.EventListener;
import org.springframework.integration.mqtt.support.MqttHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class MessageSentEventListener {
    private final NotificationPublisher publisher;

    public MessageSentEventListener(NotificationPublisher publisher, ProfileService service) {
        this.publisher = publisher;
    }


    @EventListener
    private void handleMessageSentEvent(MessageSentEvent messageSentEvent) {
        messageSentEvent.getReceivers().forEach(subscription -> {
            Message<String> message = new GenericMessage<>(
                    messageSentEvent.getChatRoom().getId(),
                    Map.of(MqttHeaders.TOPIC, "/chat/%s".formatted(subscription.getUserProfile().getId())));
            publisher.send(message);
        });
    }
}
