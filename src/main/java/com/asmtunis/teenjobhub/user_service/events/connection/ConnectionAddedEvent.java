package com.asmtunis.teenjobhub.user_service.events.connection;

import com.asmtunisie.teenjobhub.models.usermodels.Connection;
import org.springframework.context.ApplicationEvent;

public class ConnectionAddedEvent extends ApplicationEvent {

    private final Connection connection;

    public ConnectionAddedEvent(Object source, Connection connection) {
        super(source);
        this.connection = connection;
    }

    public Connection getConnection() {
        return connection;
    }
}