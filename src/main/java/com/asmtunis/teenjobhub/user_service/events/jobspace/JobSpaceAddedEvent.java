package com.asmtunis.teenjobhub.user_service.events.jobspace;

import com.asmtunisie.teenjobhub.models.jobspace.JobSpace;
import com.asmtunisie.teenjobhub.models.usermodels.Connection;
import org.springframework.context.ApplicationEvent;

public class JobSpaceAddedEvent extends ApplicationEvent {

    private final JobSpace jobSpace;

    public JobSpaceAddedEvent(Object source, JobSpace jobSpace) {
        super(source);
        this.jobSpace = jobSpace;
    }

    public JobSpace getJobSpace() {
        return jobSpace;
    }
}