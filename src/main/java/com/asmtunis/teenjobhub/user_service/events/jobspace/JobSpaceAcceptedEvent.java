package com.asmtunis.teenjobhub.user_service.events.jobspace;

import com.asmtunisie.teenjobhub.models.usermodels.UserProfile;
import org.springframework.context.ApplicationEvent;

public class JobSpaceAcceptedEvent extends ApplicationEvent {
    private final UserProfile jobSpaceOwner;

    private final UserProfile joinedUser;

    public JobSpaceAcceptedEvent(Object source,UserProfile jobSpaceOwner, UserProfile joinedUser) {
        super(source);
        this.jobSpaceOwner = jobSpaceOwner;
        this.joinedUser = joinedUser;
    }

    public UserProfile getJoinedUser() {
        return joinedUser;
    }

    public UserProfile getJobSpaceOwner() {
        return jobSpaceOwner;
    }
}
