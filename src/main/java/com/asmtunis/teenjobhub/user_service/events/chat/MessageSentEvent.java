package com.asmtunis.teenjobhub.user_service.events.chat;

import com.asmtunisie.teenjobhub.models.chat.ChatMessage;
import com.asmtunisie.teenjobhub.models.chat.ChatRoom;
import com.asmtunisie.teenjobhub.models.chat.ChatRoomSubscription;
import org.springframework.context.ApplicationEvent;

import java.util.List;

public class MessageSentEvent extends ApplicationEvent {
    private final List<ChatRoomSubscription> receivers;
    private final ChatRoom chatRoom;
    public MessageSentEvent(Object source, List<ChatRoomSubscription> receivers, ChatRoom chatRoom ) {
        super(source);
        this.receivers = receivers;
        this.chatRoom = chatRoom;
    }

    public List<ChatRoomSubscription> getReceivers() {
        return receivers;
    }

    public ChatRoom getChatRoom() {
        return chatRoom;
    }
}
