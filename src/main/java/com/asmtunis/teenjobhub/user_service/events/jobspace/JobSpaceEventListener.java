package com.asmtunis.teenjobhub.user_service.events.jobspace;

import com.asmtunis.teenjobhub.user_service.services.notification.NotificationPublisher;
import com.asmtunis.teenjobhub.user_service.services.notification.NotificationsService;
import com.asmtunisie.teenjobhub.models.jobspace.JobSpace;
import com.asmtunisie.teenjobhub.models.notifications.Notification;
import com.asmtunisie.teenjobhub.models.notifications.NotificationContext;
import org.springframework.context.event.EventListener;
import org.springframework.integration.mqtt.support.MqttHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.UUID;

@Component
public class JobSpaceEventListener {

    private final NotificationPublisher publisher;

    private final NotificationsService service;


    public JobSpaceEventListener(NotificationPublisher publisher, NotificationsService service) {
        this.publisher = publisher;
        this.service = service;
    }

    @EventListener
    public void handleJobSpaceAddedEvent(JobSpaceAddedEvent event) {

        JobSpace jobSpace = event.getJobSpace();


        jobSpace.getStaff().forEach(
                jobSpaceStaff -> {
                    Notification notification = new Notification();
                    notification.setContext(NotificationContext.JOBSPACE);
                    notification.setId(String.valueOf(UUID.randomUUID()));
                    notification.setSender(jobSpace.getAdmin());
                    notification.setMessage(
                            "%s %s Wants to add you to %s".formatted("",
                                    "", jobSpace.getName()));
                    notification.setReceiver(jobSpaceStaff.getUserProfile());
                    Message<String> message = new GenericMessage<>(
                            notification.getMessage(),
                            Map.of(MqttHeaders.TOPIC, "/notification/%s".formatted(jobSpaceStaff.getUserProfile().getUser().getId())));
                    publisher.send(message);
                    service.save(notification);

                }
        );

    }

    @EventListener
    public void handleJobSpaceAcceptedEvent(JobSpaceAcceptedEvent jobSpaceAcceptedEvent){
        Notification notification = new Notification();
        notification.setContext(NotificationContext.JOBSPACE);
        notification.setId(String.valueOf(UUID.randomUUID()));
        notification.setMessage("%s%s accepted invitation to join your job space".formatted(jobSpaceAcceptedEvent.getJoinedUser().getFirstName(), jobSpaceAcceptedEvent.getJoinedUser().getLastName()));
        Message<String> message = new GenericMessage<>(
                notification.getMessage(),
                Map.of(MqttHeaders.TOPIC, "/notification/%s".formatted(jobSpaceAcceptedEvent.getJobSpaceOwner())));
        publisher.send(message);
        service.save(notification);
    }


}
