package com.asmtunis.teenjobhub.user_service.events.connection;

import com.asmtunis.teenjobhub.user_service.services.notification.NotificationPublisher;
import com.asmtunis.teenjobhub.user_service.services.notification.NotificationsService;
import com.asmtunis.teenjobhub.user_service.util.NotificationUtils;
import com.asmtunisie.teenjobhub.models.notifications.Notification;
import com.asmtunisie.teenjobhub.models.notifications.NotificationContext;
import com.asmtunisie.teenjobhub.models.usermodels.Connection;
import com.asmtunisie.teenjobhub.models.usermodels.UserProfile;
import org.springframework.context.event.EventListener;
import org.springframework.integration.mqtt.support.MqttHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.UUID;

import static com.asmtunis.teenjobhub.user_service.util.NotificationUtils.getNotification;

@Component
public class ConnectionEventListener {

    private final NotificationPublisher publisher;
    private final NotificationsService service;

    public ConnectionEventListener(NotificationPublisher publisher, NotificationsService service) {
        this.publisher = publisher;
        this.service = service;
    }


    @EventListener
    public void handleConnectionAddedEvent(ConnectionAddedEvent event) {
        Connection connection = event.getConnection();
        Notification notification = getNotification(connection.getOriginUser(), connection.getDestinationUser(), " Requested to connect");
        Message<String> message = new GenericMessage<>(
                notification.getMessage(),
                Map.of(MqttHeaders.TOPIC, "/notification/%s".formatted(connection.getDestinationUser().getUser().getId())));
        publisher.send(message);
        service.save(notification);

    }



    @EventListener
    public void handleConnectionAcceptedEvent(ConnectionAcceptedEvent event) {
        Connection connection = event.getConnection();
        Notification notification = getNotification(connection.getDestinationUser(), connection.getOriginUser(), " accepted your request");
        Message<String> message = new GenericMessage<>(
                notification.getMessage(),
                Map.of(MqttHeaders.TOPIC, "/notification/%s".formatted(connection.getDestinationUser().getUser().getId())));
        publisher.send(message);
        service.save(notification);
    }
}
