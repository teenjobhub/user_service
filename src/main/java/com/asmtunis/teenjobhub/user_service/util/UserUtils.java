package com.asmtunis.teenjobhub.user_service.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;

public class UserUtils {
    private UserUtils() {}
    public static   String getUserIDFromToken() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null && authentication.getPrincipal() instanceof Jwt jwt) {
            return (String  ) jwt.getClaims().get("userID");
        }
        return "";
    }

}
