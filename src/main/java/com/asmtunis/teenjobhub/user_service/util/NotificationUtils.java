package com.asmtunis.teenjobhub.user_service.util;

import com.asmtunisie.teenjobhub.models.notifications.Notification;
import com.asmtunisie.teenjobhub.models.notifications.NotificationContext;
import com.asmtunisie.teenjobhub.models.usermodels.UserProfile;

import java.util.UUID;

public class NotificationUtils {
    public static Notification getNotification(UserProfile originUser, UserProfile destinationUser, String message) {
        Notification notification = new Notification();
        notification.setContext(NotificationContext.CONNEXION);
        notification.setId(String.valueOf(UUID.randomUUID()));
        notification.setMessage(  message);
        notification.setReceiver(destinationUser);
        notification.setSender(originUser);
        return notification;
    }
}
