package com.asmtunis.teenjobhub.user_service.config;

import com.nimbusds.jose.shaded.gson.internal.LinkedTreeMap;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.util.Assert;

import java.io.Serializable;


public class CustomPermissionEvaluator implements PermissionEvaluator {
    @Override
    public boolean hasPermission(
            Authentication auth, Object targetDomainObject, Object permission) {
       return getAuthorities((String) permission);
    }


    @Override
    public boolean hasPermission(
            Authentication auth, Serializable targetId, String targetType, Object permission) {
        return true;
    }

    @SuppressWarnings("unchecked")
    private boolean getAuthorities(String permission) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            Jwt jwt = (Jwt)authentication.getPrincipal();
            LinkedTreeMap<String,String> roles = (LinkedTreeMap<String, String>) jwt.getClaims().get("roles");
            if(!roles.isEmpty()){
                String role = roles.get("6602c0fb3802874840439c74");

                return role.contains(permission);
            }else {
                return false;
            }

        }else{
            return false;
        }
    }
}
