package com.asmtunis.teenjobhub.user_service.services.suggestions;

import com.asmtunisie.teenjobhub.models.usermodels.UserProfile;
import org.apache.commons.math3.ml.clustering.Clusterable;

public class ClusteredUserProfile implements Clusterable {
    UserProfile profile;

    public ClusteredUserProfile(UserProfile profile) {
        this.profile = profile;
    }

    @Override
    public double[] getPoint() {
        return new double[]{profile.getX(), profile.getY()};
    }

    public UserProfile getProfile() {
        return profile;
    }
}
