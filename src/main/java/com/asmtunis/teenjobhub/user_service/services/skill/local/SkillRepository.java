package com.asmtunis.teenjobhub.user_service.services.skill.local;

import com.asmtunisie.teenjobhub.models.skill.Skill;
import org.springframework.data.mongodb.repository.MongoRepository;

interface SkillRepository extends MongoRepository<Skill, String> {

}