package com.asmtunis.teenjobhub.user_service.services.activity_sector.local;

import com.asmtunisie.teenjobhub.models.skill.ActivitySector;
import org.springframework.data.mongodb.repository.MongoRepository;

interface ActivitySectorRepository extends MongoRepository<ActivitySector, String> {
}
