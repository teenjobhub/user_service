package com.asmtunis.teenjobhub.user_service.services.profile.local;

import com.asmtunisie.teenjobhub.models.usermodels.UserProfile;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;
 public interface UserProfileRepository extends MongoRepository<UserProfile,String> {
    Optional<UserProfile> findByUserId(String userId);

}
