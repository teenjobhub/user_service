package com.asmtunis.teenjobhub.user_service.services.profile.local;

import com.asmtunis.teenjobhub.user_service.services.elastic.ElasticUserProfileRepository;
import com.asmtunis.teenjobhub.user_service.services.file.FileContext;
import com.asmtunis.teenjobhub.user_service.services.file.FileService;
import com.asmtunis.teenjobhub.user_service.services.profile.ProfileService;
import com.asmtunis.teenjobhub.user_service.util.UserUtils;
import com.asmtunisie.teenjobhub.models.usermodels.ElasticUserProfile;
import com.asmtunisie.teenjobhub.models.usermodels.User;
import com.asmtunisie.teenjobhub.models.usermodels.UserProfile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

@Service
public class DefaultProfileService implements ProfileService {

    private final UserProfileRepository repository;
    private final ElasticUserProfileRepository elasticUserProfileRepository;
    private final FileService fileService;

    public DefaultProfileService(
            UserProfileRepository repository,
            ElasticUserProfileRepository elasticUserProfileRepository,
            FileService fileService) {
        this.repository = repository;
        this.elasticUserProfileRepository = elasticUserProfileRepository;
        this.fileService = fileService;
    }

    @Override
    public UserProfile save(UserProfile userProfile) {
        if (userProfile.getId() != null && userProfile.getId().isBlank()) {
            userProfile.setId(null);
        }
        userProfile.setFirstName(StringUtils.capitalize(userProfile.getFirstName()));
        userProfile.setLastName(StringUtils.capitalize(userProfile.getLastName()));
        userProfile.setUser(new User(UserUtils.getUserIDFromToken()));
        userProfile.calculate();
        return internalSave(userProfile);
    }

    private UserProfile internalSave(UserProfile userProfile) {
        UserProfile savedUserProfile = repository.save(userProfile);
        elasticUserProfileRepository.save(new ElasticUserProfile(savedUserProfile));
        return userProfile;
    }

    @Override
    public List<UserProfile> findAll() {
        return repository.findAll();
    }

    @Override
    public Page<ElasticUserProfile> search(String query, Pageable pageable) {
        return elasticUserProfileRepository.findProfilesByFirstName(query, pageable);
    }

    @Override
    public Optional<UserProfile> me() {
        return repository.findByUserId(UserUtils.getUserIDFromToken());
    }

    @Override
    public void updatePicture(MultipartFile file) {
        me().ifPresent(userProfile -> {
            userProfile.setUserImage(fileService.store(file, FileContext.USER_PROFILE_IMAGE));
            internalSave(userProfile);
        });
    }

    @Override
    public UserProfile updateSignature(MultipartFile file) {
        me().ifPresent(userProfile -> {
            userProfile.setUserSignature(fileService.store(file, FileContext.USER_SIGNATURE));
            internalSave(userProfile);
        });

        return me().orElse(null);

    }

    @Override
    public Optional<UserProfile> findById(String id) {
        return repository.findById(id);
    }


}
