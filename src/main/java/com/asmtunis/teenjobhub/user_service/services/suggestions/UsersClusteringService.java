package com.asmtunis.teenjobhub.user_service.services.suggestions;

import com.asmtunis.teenjobhub.user_service.services.profile.ProfileService;
import com.asmtunis.teenjobhub.user_service.util.UserUtils;
import com.asmtunisie.teenjobhub.models.usermodels.UserProfile;
import org.apache.commons.math3.ml.clustering.CentroidCluster;
import org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer;
import org.apache.commons.math3.ml.distance.EuclideanDistance;
import org.apache.commons.math3.random.JDKRandomGenerator;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UsersClusteringService {
    private final ProfileService service;

    public UsersClusteringService(ProfileService service) {
        this.service = service;
    }

    private List<CentroidCluster<ClusteredUserProfile>> clusterUsers() {
        List<UserProfile> profiles = service.findAll();
        KMeansPlusPlusClusterer<ClusteredUserProfile> tkMeansPlusPlusClusterer = new KMeansPlusPlusClusterer<>(
                18, -1,
                new EuclideanDistance(),
                new JDKRandomGenerator(),
                KMeansPlusPlusClusterer.EmptyClusterStrategy.LARGEST_VARIANCE);

        return tkMeansPlusPlusClusterer.cluster(profiles.stream().map(ClusteredUserProfile::new).toList());
    }
    private CentroidCluster<ClusteredUserProfile> getClusterForUser(UserProfile user) {
        ClusteredUserProfile clusteredUser = new ClusteredUserProfile(user);
        EuclideanDistance distanceMeasure = new EuclideanDistance();

        return clusterUsers().stream()
                .min(Comparator.comparingDouble(c -> distanceMeasure.compute(c.getCenter().getPoint(), clusteredUser.getPoint())))
                .orElse(null);
    }

    public List<UserProfile> getSuggestions( int k) {
        UserProfile userProfile =  service.me().get();

        CentroidCluster<ClusteredUserProfile> userCluster = getClusterForUser(userProfile);
        if (userCluster == null) {
            return List.of();
        }

        return userCluster.getPoints().stream()
                .map(ClusteredUserProfile::getProfile)
                .filter(profile -> !profile.getId().equals(userProfile.getId()))
                .limit(k)
                .collect(Collectors.toList());
    }


}
