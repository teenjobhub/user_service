package com.asmtunis.teenjobhub.user_service.services.elastic;

import com.asmtunisie.teenjobhub.models.usermodels.ElasticUserProfile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ElasticUserProfileRepository extends ElasticsearchRepository<ElasticUserProfile, String> {

    @Query("{\"bool\": {\"must\": [{\"match\": {\"firstName\": \"?0\"}}]}}")
    Page<ElasticUserProfile> findProfilesByFirstName(String name, Pageable pageable);
}
