package com.asmtunis.teenjobhub.user_service.services.occupation.local;

import com.asmtunis.teenjobhub.user_service.services.occupation.OccupationService;
import com.asmtunisie.teenjobhub.models.skill.Occupation;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class DefaultOccupationService implements OccupationService {
    private final OccupationRepository repository;

    public DefaultOccupationService(OccupationRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Occupation> findAll() {
        return repository.findAll();
    }

    @Override
    public Optional<Occupation> findById(String id) {
        return repository.findById(id);
    }

    @Override
    public void save(Occupation occupation) {
        repository.save(occupation);
    }

    @Override
    public void save(Collection<Occupation> occupations) {
        repository.saveAll(occupations);
    }
}
