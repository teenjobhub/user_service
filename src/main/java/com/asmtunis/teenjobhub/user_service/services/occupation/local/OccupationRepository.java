package com.asmtunis.teenjobhub.user_service.services.occupation.local;

import com.asmtunisie.teenjobhub.models.skill.Occupation;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface OccupationRepository extends MongoRepository<Occupation,String> {

}
