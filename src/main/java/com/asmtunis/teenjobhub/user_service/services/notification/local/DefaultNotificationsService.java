package com.asmtunis.teenjobhub.user_service.services.notification.local;

import com.asmtunis.teenjobhub.user_service.services.notification.NotificationsService;
import com.asmtunis.teenjobhub.user_service.util.UserUtils;
import com.asmtunisie.teenjobhub.models.notifications.Notification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultNotificationsService implements NotificationsService {

    private final NotificationRepository notificationRepository;

    public DefaultNotificationsService(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    @Override
    public Notification save(Notification notification) {
        return notificationRepository.save(notification);
    }

    @Override
    public List<Notification> findByUserID() {
        return notificationRepository.findAll()
                .stream()
                .filter(notification -> notification.getReceiver().getUser().getId().equals(UserUtils.getUserIDFromToken())).toList();
    }
}
