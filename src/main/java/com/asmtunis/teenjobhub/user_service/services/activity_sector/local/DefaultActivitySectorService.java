package com.asmtunis.teenjobhub.user_service.services.activity_sector.local;

import com.asmtunis.teenjobhub.user_service.services.activity_sector.ActivitySectorService;
import com.asmtunisie.teenjobhub.models.skill.ActivitySector;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultActivitySectorService implements ActivitySectorService {
    private final ActivitySectorRepository repository;

    public DefaultActivitySectorService(ActivitySectorRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<ActivitySector> saveAll(Iterable<ActivitySector> activitySectors) {
        return repository.saveAll(activitySectors);
    }

    @Override
    public ActivitySector save(ActivitySector activitySector) {
        return repository.save(activitySector);
    }

    @Override
    public List<ActivitySector> findAll() {
        return repository.findAll();
    }
}
