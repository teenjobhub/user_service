package com.asmtunis.teenjobhub.user_service.services.profile;

import com.asmtunisie.teenjobhub.models.usermodels.ElasticUserProfile;
import com.asmtunisie.teenjobhub.models.usermodels.User;
import com.asmtunisie.teenjobhub.models.usermodels.UserProfile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

public interface ProfileService {
    UserProfile save(UserProfile userProfile);

    List<UserProfile> findAll();

    Page<ElasticUserProfile> search(String query, Pageable pageable);

    Optional<UserProfile> me();

    void updatePicture(MultipartFile file);

    UserProfile updateSignature(MultipartFile file);

    Optional<UserProfile> findById(String id);
}
