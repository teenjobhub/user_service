package com.asmtunis.teenjobhub.user_service.services.network.local;

import com.asmtunis.teenjobhub.user_service.events.connection.ConnectionAcceptedEvent;
import com.asmtunis.teenjobhub.user_service.events.connection.ConnectionAddedEvent;
import com.asmtunis.teenjobhub.user_service.services.network.NetworkService;
import com.asmtunis.teenjobhub.user_service.services.profile.ProfileService;
import com.asmtunis.teenjobhub.user_service.util.UserUtils;
import com.asmtunisie.teenjobhub.models.usermodels.Connection;
import com.asmtunisie.teenjobhub.models.usermodels.UserProfile;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;

@Service
public class DefaultNetworkService implements NetworkService {

    private final NetworkRepository repository;

    private final ProfileService profileService;

    private final ApplicationEventPublisher publisher;

    public DefaultNetworkService(NetworkRepository repository, ProfileService profileService, ApplicationEventPublisher publisher) {
        this.repository = repository;
        this.profileService = profileService;
        this.publisher = publisher;
    }

    @Override
    public void addConnection(String destinationUserId) {
        Connection connection = new Connection();
        profileService.findById(destinationUserId).ifPresent(connection::setDestinationUser);
        profileService.me().ifPresent(connection::setOriginUser);
        repository.insert(connection);
        CompletableFuture.runAsync(() -> {
            publisher.publishEvent(new ConnectionAddedEvent(this, connection));
        });
    }

    @Override
    public Page<Connection> getSentRequests(Pageable pageable) {
        return profileService.me().map(userProfile -> repository.mySentRequests(userProfile.getId(), pageable)).orElseThrow();
    }

    @Override
    public Page<Connection> getReceivedRequests(Pageable pageable) {
        return profileService.me().map(userProfile -> repository.myReceivedRequests(userProfile.getId(), pageable)).orElseThrow();
    }

    @Override
    public boolean handleRequest(String requestId, boolean decision) {
        Connection connection = repository.findById(requestId).orElseThrow();

        if (Objects.equals(connection.getDestinationUser().getUser().getId(), UserUtils.getUserIDFromToken())) {
            return decideConnectionStatus(decision, connection);
        } else {
            throw new IllegalArgumentException("connection not found");
        }
    }

    @Override
    public Page<Connection> findMyConnections(Pageable pageable) {
        return profileService.me()
                .map(userProfile -> getConnections(pageable, userProfile))
                .orElseThrow();
    }

    @Override
    public Page<Connection> findUserConnections(String userProfileId, Pageable pageable) {
        return profileService.findById(userProfileId)
                .map(userProfile -> getConnections(pageable, userProfile))
                .orElseThrow();
    }

    private Page<Connection> getConnections(Pageable pageable, @NonNull UserProfile userProfile) {
        return repository.myNetwork(userProfile.getId(), pageable);
    }

    private boolean decideConnectionStatus(boolean userDecision, @NonNull Connection connection) {
        if (userDecision) {
            connection.setPending(false);
            repository.save(connection);
            CompletableFuture.runAsync(() -> publisher.publishEvent(new ConnectionAcceptedEvent(this, connection)));
            return true;
        } else {
            repository.delete(connection);
            return false;
        }
    }
}
