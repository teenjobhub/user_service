package com.asmtunis.teenjobhub.user_service.services.occupation;

import com.asmtunisie.teenjobhub.models.skill.Occupation;

import java.util.Collection;
import java.util.List;
import java.util.Optional;


public interface OccupationService {
    List<Occupation> findAll();

    Optional<Occupation> findById(String id);

    void save(Occupation occupation);
    void save(Collection<Occupation> occupations);
}
