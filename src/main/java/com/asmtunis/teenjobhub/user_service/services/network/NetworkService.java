package com.asmtunis.teenjobhub.user_service.services.network;

import com.asmtunisie.teenjobhub.models.usermodels.Connection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface NetworkService {

    void addConnection(String destinationUserId);
    Page<Connection> getSentRequests(Pageable pageable);
    Page<Connection> getReceivedRequests(Pageable pageable);
    boolean handleRequest(String requestId, boolean decision);
    Page<Connection> findMyConnections(Pageable pageable);
    Page<Connection> findUserConnections(String userProfileId, Pageable pageable);
}
