package com.asmtunis.teenjobhub.user_service.services.chat;

import com.asmtunisie.teenjobhub.models.chat.ChatMessage;
import com.asmtunisie.teenjobhub.models.chat.ChatRoom;
import com.asmtunisie.teenjobhub.models.usermodels.UserProfile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ChatService {

    void sendMessage(String chatRoomId, String message);


    ChatRoom getChatRoom(String chatRoomOrUserProfileId);

    ChatRoom getChatRoom(UserProfile him, UserProfile me);

    Page<ChatMessage> getMessages(String chatRoomId, Pageable pageable);

    Page<ChatRoom> getChatRooms(Pageable pageable);
}
