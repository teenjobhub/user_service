package com.asmtunis.teenjobhub.user_service.services.skill;

import com.asmtunisie.teenjobhub.models.skill.Skill;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface SkillService {
    Page<Skill> findAll(Pageable pageable);
    void save(Iterable<Skill> skills);
}
