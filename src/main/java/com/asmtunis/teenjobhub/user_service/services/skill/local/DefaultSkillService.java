package com.asmtunis.teenjobhub.user_service.services.skill.local;

import com.asmtunis.teenjobhub.user_service.services.skill.SkillService;
import com.asmtunisie.teenjobhub.models.skill.Skill;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class DefaultSkillService implements SkillService {

    private final SkillRepository skillRepository;

    public DefaultSkillService(SkillRepository skillRepository) {
        this.skillRepository = skillRepository;
    }

    @Override
    public Page<Skill> findAll(Pageable pageable) {
        return skillRepository.findAll(pageable);
    }

    @Override
    public void save(Iterable<Skill> skills) {
        skillRepository.saveAll(skills);
    }
}


