package com.asmtunis.teenjobhub.user_service.services.jobspace.local;

import com.asmtunisie.teenjobhub.models.jobspace.JobSpace;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Collection;
import java.util.Optional;

interface JobSpaceRepository extends MongoRepository<JobSpace,String> {
    @Query("{staff: {$elemMatch: {\"pending\": ?1, \"userProfile\": ?0}}}")
    Collection<JobSpace> findMyJobSpaces(String userProfileId, boolean pending);
}
