package com.asmtunis.teenjobhub.user_service.services.network.local;

import com.asmtunisie.teenjobhub.models.usermodels.Connection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

interface NetworkRepository extends MongoRepository<Connection,String> {

    @Query("{$and: [{pending: false}, {$or :[{originUser: ?0},{destinationUser: ?0}]}]}")
    Page<Connection> myNetwork(String userProfileId, Pageable pageable);

    @Query("{$and: [{pending: true}, {$or :[{destinationUser: ?0}]}]}")
    Page<Connection> myReceivedRequests(String userProfileId, Pageable pageable);

    @Query("{$and: [{pending: true}, {$or :[{originUser: ?0}]}]}")
    Page<Connection> mySentRequests(String userProfileId, Pageable pageable);

}
