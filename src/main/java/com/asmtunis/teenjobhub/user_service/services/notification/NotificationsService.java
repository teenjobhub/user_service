package com.asmtunis.teenjobhub.user_service.services.notification;

import com.asmtunisie.teenjobhub.models.notifications.Notification;

import java.util.List;

public interface NotificationsService {

    Notification save(Notification notification);

    List<Notification> findByUserID();
}
