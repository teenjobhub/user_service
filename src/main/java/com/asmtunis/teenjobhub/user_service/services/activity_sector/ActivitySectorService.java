package com.asmtunis.teenjobhub.user_service.services.activity_sector;

import com.asmtunisie.teenjobhub.models.skill.ActivitySector;

import java.util.List;

public interface ActivitySectorService {

    List<ActivitySector> saveAll(Iterable<ActivitySector> activitySectors);
    ActivitySector save(ActivitySector activitySector);
    List<ActivitySector>  findAll();
}
