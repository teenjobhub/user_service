package com.asmtunis.teenjobhub.user_service.services.jobspace.local;

import com.asmtunis.teenjobhub.user_service.events.connection.ConnectionAddedEvent;
import com.asmtunis.teenjobhub.user_service.events.jobspace.JobSpaceAddedEvent;
import com.asmtunis.teenjobhub.user_service.services.jobspace.JobSpaceService;
import com.asmtunis.teenjobhub.user_service.services.profile.ProfileService;
import com.asmtunisie.teenjobhub.models.jobspace.JobSpace;
import com.asmtunisie.teenjobhub.models.usermodels.JobSpaceStaff;
import com.asmtunisie.teenjobhub.models.usermodels.UserProfile;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
public class DefaultJobSpaceService implements JobSpaceService {

    private final JobSpaceRepository repository;
    private final ProfileService profileService;

    private final ApplicationEventPublisher publisher;

    public DefaultJobSpaceService(JobSpaceRepository repository, ProfileService profileService, ApplicationEventPublisher publisher) {
        this.repository = repository;
        this.profileService = profileService;
        this.publisher = publisher;
    }

    @Override
    public JobSpace save(JobSpace jobSpace) {
        jobSpace.setId(String.valueOf(UUID.randomUUID()));

        CompletableFuture.runAsync(() -> {
            publisher.publishEvent(new JobSpaceAddedEvent(this, jobSpace));
        });
        return repository.save(jobSpace);
    }

    @Override
    public Optional<JobSpace> findById(String id) {
        return repository.findById(id);
    }

    @Override
    public void addUser(String id, String userProfileId) {
        JobSpace jobSpace = repository.findById(id).orElseThrow(() -> new IllegalArgumentException("job space not found"));
        jobSpace.getStaff().add(new JobSpaceStaff(new UserProfile(userProfileId), true));
        repository.save(jobSpace);
    }

    @Override
    public boolean handleInvitation(String id, String userProfileId, boolean decision) {
        JobSpace jobSpace = repository.findById(id).orElseThrow();
        return decision ?
                acceptJobSpace(userProfileId, jobSpace) :
                denyJobSpace(userProfileId, jobSpace);
    }

    @Override
    public Collection<JobSpace> findMyJobSpaces() {
        UserProfile me = profileService.me().orElseThrow();

        List<JobSpace> jobSpaces = repository.findAll().stream()
                .filter(jobSpace -> jobSpace.getAdmin().getId().equals(me.getId()))
                .collect(Collectors.toList());
        jobSpaces.addAll( repository.findMyJobSpaces(me.getId(), false));
        return jobSpaces;
    }

    @Override
    public Collection<JobSpace> findMyInvitations() {
        UserProfile me = profileService.me().orElseThrow();
        return repository.findMyJobSpaces(me.getId(), true);
    }

    private boolean acceptJobSpace(String userProfileID, JobSpace jobSpace) {
        return jobSpace
                .getStaff()
                .stream()
                .filter(jobSpaceStaff1 -> jobSpaceStaff1.getUserProfile().getId().equals(userProfileID))
                .peek(jobSpaceStaff -> jobSpaceStaff.setPending(false))
                .findFirst()
                .map(jobSpaceStaff -> {
                    repository.save(jobSpace);
                    return true;
                }).orElse(false);
    }

    private boolean denyJobSpace(String userProfileID, JobSpace jobSpace) {
        if (jobSpace
                .getStaff().removeIf(jobSpaceStaff -> Objects.equals(jobSpaceStaff.getUserProfile().getId(), userProfileID))) {
            repository.save(jobSpace);
            return true;
        }
        return false;

    }
}
