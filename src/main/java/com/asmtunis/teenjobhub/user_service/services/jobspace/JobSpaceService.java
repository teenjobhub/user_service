package com.asmtunis.teenjobhub.user_service.services.jobspace;

import com.asmtunisie.teenjobhub.models.jobspace.JobSpace;

import java.util.Collection;
import java.util.Optional;

public interface JobSpaceService {

    JobSpace save(JobSpace jobSpace);

    Optional<JobSpace> findById(String id);

    void addUser(String id, String userProfileId);

    boolean handleInvitation(String id, String userProfileId, boolean decision);

    Collection<JobSpace> findMyJobSpaces();
    Collection<JobSpace> findMyInvitations();

}
