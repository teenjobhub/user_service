package com.asmtunis.teenjobhub.user_service.services.chat.local;

import com.asmtunis.teenjobhub.user_service.events.chat.MessageSentEvent;
import com.asmtunis.teenjobhub.user_service.services.chat.ChatService;
import com.asmtunis.teenjobhub.user_service.services.profile.ProfileService;
import com.asmtunisie.teenjobhub.models.chat.ChatMessage;
import com.asmtunisie.teenjobhub.models.chat.ChatRoom;
import com.asmtunisie.teenjobhub.models.chat.ChatRoomSubscription;
import com.asmtunisie.teenjobhub.models.usermodels.UserProfile;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

@Service
public class DefaultChatService implements ChatService {
    private final ChatRoomRepository chatRoomRepository;
    private final ChatMessageRepository chatMessageRepository;

    private final ProfileService profileService;

    private final ApplicationEventPublisher publisher;



    public DefaultChatService(ChatRoomRepository chatRoomRepository, ChatMessageRepository chatMessageRepository, ProfileService profileService, ApplicationEventPublisher publisher) {
        this.chatRoomRepository = chatRoomRepository;
        this.chatMessageRepository = chatMessageRepository;
        this.profileService = profileService;
        this.publisher = publisher;
    }

    @Override
    public void sendMessage(String chatRoomId, String message) {
        UserProfile me = profileService.me().orElseThrow();
        ChatRoom chatRoom = getChatRoom(chatRoomId);
        ChatMessage draft = new ChatMessage();
        draft.setBody(message);
        draft.setChatRoom(chatRoom);
        draft.setSender(me);
        ChatMessage chatMessage = chatMessageRepository.insert(draft);
        chatRoom.setLast(chatMessage);
        chatRoom.getMembers().stream().filter(chatRoomSubscription -> Objects.equals(chatRoomSubscription.getUserProfile().getId(), me.getId())).forEach(chatRoomSubscription -> chatRoomSubscription.setLastSeenMessage(chatMessage));

        chatRoomRepository.save(chatRoom);
        List<ChatRoomSubscription> receivers = chatRoom.getMembers().stream().filter(subscription -> !Objects.equals(subscription.getUserProfile().getId(), me.getId())).toList();
        CompletableFuture.runAsync(() -> {
            publisher.publishEvent(new MessageSentEvent(this, receivers,chatRoom));
        });
    }

    @Override
    public ChatRoom getChatRoom(String chatRoomOrUserProfileId) {
        UserProfile me = profileService.me().orElseThrow();
        ChatRoom room = chatRoomRepository.findById(chatRoomOrUserProfileId).orElseGet(() -> chatRoomRepository.findByUserProfileIds(List.of(chatRoomOrUserProfileId, me.getId())).orElseGet(() -> {
            ChatRoom chatRoom = new ChatRoom();
            UserProfile him = profileService.findById(chatRoomOrUserProfileId).orElseThrow();
            ChatRoomSubscription mySubscription = new ChatRoomSubscription();
            mySubscription.setUserProfile(me);
            ChatRoomSubscription hisSubscription = new ChatRoomSubscription();
            hisSubscription.setUserProfile(him);
            chatRoom.setMembers(List.of(mySubscription, hisSubscription));
            return chatRoomRepository.save(chatRoom);
        }));
        boolean unread = room.getLast() != null && room.getMembers()
                .stream()
                .filter(subscription -> Objects.equals(subscription.getUserProfile().getId(), me.getId()))
                .map(ChatRoomSubscription::getLastSeenMessage)
                .filter(Objects::nonNull)
                .map(ChatMessage::getId)
                .noneMatch(room.getLast().getId()::equals);
        room.setUnread(unread);
        return room;
    }

    @Override
    public ChatRoom getChatRoom(UserProfile him, UserProfile me) {
        return chatRoomRepository.findByUserProfileIds(List.of(him.getId(), me.getId())).orElseGet(() -> {
            ChatRoom chatRoom = new ChatRoom();
            ChatRoomSubscription mySubscription = new ChatRoomSubscription();
            mySubscription.setUserProfile(me);
            ChatRoomSubscription hisSubscription = new ChatRoomSubscription();
            hisSubscription.setUserProfile(him);
            chatRoom.setMembers(List.of(mySubscription, hisSubscription));
            return chatRoomRepository.save(chatRoom);
        });
    }
    @Override
    public Page<ChatMessage> getMessages(String chatRoomId, Pageable pageable) {
        UserProfile me = profileService.me().orElseThrow();
        ChatRoom chatRoom = getChatRoom(chatRoomId);
        chatRoom.getMembers().stream().filter(chatRoomSubscription -> Objects.equals(chatRoomSubscription.getUserProfile().getId(), me.getId())).forEach(chatRoomSubscription -> chatRoomSubscription.setLastSeenMessage(chatRoom.getLast()));
        chatRoomRepository.save(chatRoom);
        long t = System.nanoTime();
        Page<ChatMessage> byChatRoomId = chatMessageRepository.findByChatRoomId(chatRoomId, pageable);
        System.out.println(System.nanoTime() - t);
        return byChatRoomId;
    }

    @Override
    public Page<ChatRoom> getChatRooms(Pageable pageable) {
        UserProfile me = profileService.me().orElseThrow();
        return chatRoomRepository.findByUserProfileId(me.getId(), pageable);
    }
}
