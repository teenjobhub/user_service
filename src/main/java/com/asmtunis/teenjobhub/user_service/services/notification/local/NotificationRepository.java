package com.asmtunis.teenjobhub.user_service.services.notification.local;

import com.asmtunisie.teenjobhub.models.notifications.Notification;
import org.springframework.data.mongodb.repository.MongoRepository;
interface NotificationRepository extends MongoRepository<Notification,String> {
}
