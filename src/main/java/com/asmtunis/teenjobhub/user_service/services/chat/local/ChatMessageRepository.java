package com.asmtunis.teenjobhub.user_service.services.chat.local;

import com.asmtunisie.teenjobhub.models.chat.ChatMessage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Optional;

public interface ChatMessageRepository extends MongoRepository<ChatMessage,String> {
    @Query(value = "{chatRoom: ?0}", fields = "id body sender.id sender.name sender.userImage")
    Page<ChatMessage> findByChatRoomId(String id, Pageable pageable);
}
