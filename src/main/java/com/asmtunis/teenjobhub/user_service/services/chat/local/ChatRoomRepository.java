package com.asmtunis.teenjobhub.user_service.services.chat.local;

import com.asmtunisie.teenjobhub.models.chat.ChatRoom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ChatRoomRepository extends MongoRepository<ChatRoom,String> {

    @Query("{ 'members.userProfile.id': ?0 }")
    Page<ChatRoom> findByUserProfileId(String id, Pageable pageable);

    @Query("{ 'members.userProfile.id': { $all: ?0} }")
    Optional<ChatRoom> findByUserProfileIds(List<String> ids);
}
