package com.asmtunis.teenjobhub.user_service.controllers;

import com.asmtunis.teenjobhub.user_service.services.file.FileService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;

@Controller
@RequestMapping("file")
public class FileController {

    private final FileService service;

    public FileController(FileService service) {
        this.service = service;
    }

    @GetMapping("**")
    public void loadFile(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String servletPath = request.getServletPath().substring(5);
        Pair<String, byte[]> load = service.load(servletPath);
        response.setContentType(load.getFirst());
        response.getOutputStream().write(load.getSecond());
        response.flushBuffer();
    }
}
