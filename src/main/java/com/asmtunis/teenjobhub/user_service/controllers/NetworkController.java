package com.asmtunis.teenjobhub.user_service.controllers;

import com.asmtunis.teenjobhub.user_service.services.network.NetworkService;
import com.asmtunis.teenjobhub.user_service.services.profile.local.UserProfileRepository;
import com.asmtunis.teenjobhub.user_service.services.suggestions.UsersClusteringService;
import com.asmtunis.teenjobhub.user_service.util.UserUtils;
import com.asmtunisie.teenjobhub.models.usermodels.UserProfile;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/network")
public class NetworkController {

    final NetworkService networkService;
    final UsersClusteringService usersClusteringService;
    final UserProfileRepository userProfileRepository;
    public NetworkController(NetworkService networkService, UsersClusteringService usersClusteringService, UserProfileRepository userProfileRepository) {
        this.networkService = networkService;
        this.usersClusteringService = usersClusteringService;
        this.userProfileRepository = userProfileRepository;
    }

    @PutMapping
    public ResponseEntity<?> addConnection(@RequestParam String destinationUserID) {
        networkService.addConnection(destinationUserID);
        return ResponseEntity.ok(true);
    }

    @PostMapping
    public ResponseEntity<Boolean> acceptOrDenyConnectionRequest(@RequestParam boolean userDecision, @RequestParam String connectionID) {
        return ResponseEntity.ok(networkService.handleRequest(connectionID, userDecision));
    }

    @GetMapping("getConnectionRequest")
    public ResponseEntity<?> getConnectionRequest(Pageable pageable) {
        return ResponseEntity.ok(networkService.getSentRequests(pageable));
    }

    @GetMapping("getUserConnections")
    public ResponseEntity<?> getUserConnections(Pageable pageable) {
        return ResponseEntity.ok(networkService.findMyConnections(pageable));
    }

    @GetMapping("getUserReceivedRequests")
    public ResponseEntity<?> getUserReceivedRequests(Pageable pageable) {
        return ResponseEntity.ok(networkService.getReceivedRequests(pageable));
    }
    @GetMapping("suggestions")
    public ResponseEntity<?> suggestions() {
        return ResponseEntity.ok(usersClusteringService.getSuggestions(10));
    }


}
