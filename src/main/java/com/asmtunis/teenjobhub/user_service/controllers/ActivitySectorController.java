package com.asmtunis.teenjobhub.user_service.controllers;

import com.asmtunis.teenjobhub.user_service.services.activity_sector.ActivitySectorService;

import com.asmtunisie.teenjobhub.models.skill.ActivitySector;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("api/activity-sector")
public class ActivitySectorController {

    private final ActivitySectorService service;


    public ActivitySectorController(ActivitySectorService service) {
        this.service = service;

    }

    @PutMapping
    public ResponseEntity<List<ActivitySector>> saveAll(@RequestBody Collection<ActivitySector> activitySectors) {
        return ResponseEntity.ok(service.saveAll(activitySectors));
    }

}
