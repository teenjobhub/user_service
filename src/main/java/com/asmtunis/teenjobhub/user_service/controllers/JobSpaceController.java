package com.asmtunis.teenjobhub.user_service.controllers;

import com.asmtunis.teenjobhub.user_service.services.jobspace.JobSpaceService;
import com.asmtunisie.teenjobhub.models.jobspace.JobSpace;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/jobSpace")

public class JobSpaceController {
    final JobSpaceService jobSpaceService;

    public JobSpaceController(JobSpaceService jobSpaceService) {
        this.jobSpaceService = jobSpaceService;
    }

    @PutMapping()
    public ResponseEntity<?> createJobSpace(@RequestBody JobSpace jobSpace) {
        return ResponseEntity.ok(jobSpaceService.save(jobSpace));
    }

    @PostMapping("addUserToJobSpace")
    public ResponseEntity<?> addUserToJobSpace(@RequestParam String jobSpaceID, @RequestParam String userProfileId) {
        jobSpaceService.addUser(jobSpaceID, userProfileId);
        return ResponseEntity.ok(true);
    }

    @PostMapping("acceptOrDenyInvitation")
    public ResponseEntity<?> acceptOrDenyInvitation(String jobSpaceID, String userProfileID, boolean userDecision) {
        return ResponseEntity.ok(jobSpaceService.handleInvitation(jobSpaceID, userProfileID, userDecision));
    }

    @GetMapping("getUserJobSpaces")
    public ResponseEntity<?> getUserJobSpaces() {
        return ResponseEntity.ok(jobSpaceService.findMyJobSpaces());
    }

    @GetMapping("getUserPendingJobSpaceInvitations")
    public ResponseEntity<?> getUserPendingJobSpaceInvitations() {
        return ResponseEntity.ok(jobSpaceService.findMyInvitations());
    }


}
