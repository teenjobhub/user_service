package com.asmtunis.teenjobhub.user_service.controllers;

import com.asmtunis.teenjobhub.user_service.services.chat.ChatService;
import com.asmtunisie.teenjobhub.models.chat.ChatMessage;
import com.asmtunisie.teenjobhub.models.chat.ChatRoom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/chat")
public class ChatController {
    private final ChatService service;

    public ChatController(ChatService service) {
        this.service = service;
    }

    @PutMapping
    public ResponseEntity<Boolean> sendMessage(@RequestParam String chatRoomId, @RequestBody String message) {
        service.sendMessage(chatRoomId, message);
        return ResponseEntity.ok(true);
    }

    @GetMapping("{chatRoomOrUserProfileId}")
    public ResponseEntity<ChatRoom> getChatRoom(@PathVariable String chatRoomOrUserProfileId) {
        ChatRoom room = service.getChatRoom(chatRoomOrUserProfileId);
        return ResponseEntity.ok(room);
    }

    @GetMapping("messages")
    public ResponseEntity<Page<ChatMessage>> getMessages(@RequestParam String chatRoomId, Pageable pageable) {
        pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Direction.DESC, "id"));
        Page<ChatMessage> messages = service.getMessages(chatRoomId, pageable);
        return ResponseEntity.ok(messages);
    }
    @GetMapping()
    public ResponseEntity<Page<ChatRoom>> getChatRooms(Pageable pageable) {
        pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Direction.DESC, "last.id"));
        Page<ChatRoom> messages = service.getChatRooms(pageable);
        return ResponseEntity.ok(messages);
    }
}
