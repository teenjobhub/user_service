package com.asmtunis.teenjobhub.user_service.controllers;

import com.asmtunis.teenjobhub.user_service.services.occupation.OccupationService;
import com.asmtunisie.teenjobhub.models.payload.request.OccupationRequest;
import com.asmtunisie.teenjobhub.models.payload.response.MessageResponse;
import com.asmtunisie.teenjobhub.models.skill.ActivitySector;
import com.asmtunisie.teenjobhub.models.skill.Occupation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/occupation")
public class OccupationController {
    final OccupationService service;
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public OccupationController(OccupationService service) {
        this.service = service;
    }


    @PutMapping()
    public ResponseEntity<?> addOccupations(@RequestBody List<OccupationRequest> occupationRequestList) {
        try {
            List<Occupation> occupations = new ArrayList<>();
            occupationRequestList.forEach(occupationRequest -> occupations.add(new Occupation(occupationRequest.getName())));

            service.save(occupations);
            return ResponseEntity.ok(new MessageResponse("Occupations added successfully!"));

        } catch (HttpClientErrorException.Unauthorized | HttpClientErrorException.BadRequest e) {
            logger.error("Error while adding Occupations", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Error adding Occupations: " + e.getMessage());
        }

    }

    @GetMapping()
    public ResponseEntity<?> getOccupations() {
        try {
            return ResponseEntity.ok(service.findAll());
        } catch (HttpClientErrorException.Unauthorized e) {
            return ResponseEntity.ok(new MessageResponse(e.getMessage()));
        }
    }

    @PostMapping()
    public ResponseEntity<?> updateOccupationStatus(@RequestParam String occupationID, @RequestParam String activitySectorId) {

        //TODO add to service
        service.findById(occupationID).ifPresent(occupation -> {
            if (occupation.getActivitySector() == null) {
                occupation.setActivitySector(new ActivitySector(activitySectorId));
                service.save(occupation);
            }
        });
        return ResponseEntity.ok(new MessageResponse("Occupations updated"));

    }

}
