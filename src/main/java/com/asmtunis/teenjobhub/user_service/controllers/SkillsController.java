package com.asmtunis.teenjobhub.user_service.controllers;

import com.asmtunis.teenjobhub.user_service.services.skill.SkillService;
import com.asmtunisie.teenjobhub.models.payload.request.SkillRequest;
import com.asmtunisie.teenjobhub.models.payload.response.MessageResponse;
import com.asmtunisie.teenjobhub.models.skill.Skill;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/skill")
public class SkillsController {
    final SkillService service;
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public SkillsController(SkillService service) {
        this.service = service;
    }

    @PostMapping()
    @PreAuthorize("hasPermission()")
    public ResponseEntity<?> save(@RequestBody List<SkillRequest> skills) {
        service.save(skills.stream().map(SkillRequest::getName).map(Skill::new).toList());
        return ResponseEntity.ok(new MessageResponse("Skill added successfully!"));
    }

    @GetMapping()
    public ResponseEntity<Page<Skill>> find(Pageable pageable) {
        return ResponseEntity.ok(service.findAll(pageable));
    }
}
