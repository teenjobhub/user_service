package com.asmtunis.teenjobhub.user_service.controllers;

import com.asmtunis.teenjobhub.user_service.services.notification.NotificationsService;
import com.asmtunisie.teenjobhub.models.notifications.Notification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
@RequestMapping("/api/notification")
public class NotificationController {

    private final NotificationsService notificationsService;

    public NotificationController(NotificationsService notificationsService) {
        this.notificationsService = notificationsService;
    }

    @GetMapping("findByUserID")
    public ResponseEntity<List<Notification>> findByUserID(){
        return ResponseEntity.ok(notificationsService.findByUserID());
    }
}
