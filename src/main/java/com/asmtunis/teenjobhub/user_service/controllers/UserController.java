package com.asmtunis.teenjobhub.user_service.controllers;

import com.asmtunis.teenjobhub.user_service.services.profile.ProfileService;
import com.asmtunisie.teenjobhub.models.usermodels.UserProfile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


@RestController
@RequestMapping("/api/user")
public class UserController {


    private final ProfileService profileService;

    public UserController(ProfileService profileService) {
        this.profileService = profileService;
    }

    @PutMapping()
    public ResponseEntity<?> addUserProfile(@RequestBody UserProfile userProfile )  {
        return ResponseEntity.ok(profileService.save(userProfile));
    }

    @PostMapping
    public ResponseEntity<?> updateUserImage(@RequestPart MultipartFile userImage) throws IOException {
        profileService.updatePicture(userImage);
        return ResponseEntity.ok(true);
    }

    @PostMapping("updateSignature")
    public ResponseEntity<?> updateSignature(@RequestPart MultipartFile userSignature) throws IOException {
        UserProfile userProfile = profileService.updateSignature(userSignature);
        return ResponseEntity.ok(userProfile);
    }

    @GetMapping()
    public ResponseEntity<?> me() {
           return ResponseEntity.ok(profileService.me());
    }

    @GetMapping("search")
    public ResponseEntity<?> search(@RequestParam String query, Pageable pageable) {
        return ResponseEntity.ok(profileService.search(query,pageable));
    }

    @GetMapping("findById")
    public ResponseEntity<?> findById(@RequestParam String userProfileId) {
        return ResponseEntity.ok(profileService.findById(userProfileId));
    }
}
